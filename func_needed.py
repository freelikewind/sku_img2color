import cv2
import numpy as np
from keras.models import load_model
#参数设置



def get_rgb(img_name,RESIZE,START,END):     #输入图片名字，返回中心区域rgb元组
    sku = cv2.imread(img_name)
    yuzhi= 240
    threshould=0.6
    #sku = cv2.cvtColor(sku, cv2.COLOR_BGR2HSV)
    sku = cv2.resize(sku, (RESIZE, RESIZE))
    sku = sku[START:END, START:END, :]
    white_cou=0
    for x in range(END-START):
        for y in range(END-START):
            if sku[x][y][0]>yuzhi and sku[x][y][1]>yuzhi and sku[x][y][1]>yuzhi:
                white_cou+=1
    if white_cou/(END-START)/(END-START)>threshould:
        return (255,255,255)
    return (np.mean(sku[:, :, 2]),np.mean(sku[:, :, 1]),np.mean(sku[:, :, 0]))


def get_top3(rgb_array,model):  #传入rgb数组，即相对于模型的数据，,以及rgb2color模型
    pre = model.predict(rgb_array)


    return pre.argsort(axis=1)[:,[-1,-2,-3]]  #返回的array，shape=（batch，3），每行为[rank_max,rank_mid,rank_min]


