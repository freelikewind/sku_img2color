from func_needed import get_top3
from os import  listdir
from keras.models import load_model
from func_needed import get_rgb
import numpy as np
class Sku2Color:   #通过文件夹，将里面的sku图片映射至对应color
    def __init__(self):
        self.color=['red ','dark red','light pink','pink','white','black','gray','sky blue','blue','dark blue','light purple','purple ','purplish red','light green','green','army green','bright yellow','yellow','orange','light brown','dark brown','khaki','silver ']
        self.model=load_model('rgb_model2.h5')

    def fit(self,imgfolder):   #将文件夹和代码放在同一个目录下，传入文件名即可

        self.rgbarray=[]
        self.imgnamelist=listdir('./'+imgfolder)
        for item in self.imgnamelist:
            self.rgbarray.append(get_rgb('./'+imgfolder+'/'+item,100,40,60))
        self.rgbarray =np.array(self.rgbarray)
        self.rank=get_top3(self.rgbarray,self.model)

        print('analysis over,please check!')

    def show(self,imgname):
        index=self.imgnamelist.index(imgname)
        print([self.color[item] for item in self.rank[index]])














